# REACT WORLD CHAMPIONS APP

Single page web application that shows F1 world champions from 2005 to 2015.

## Getting started
 - `git clone git@gitlab.com:gecko-ict/react-world-champions-app.git`
 - `cd react-world-champions-app`

## Run in development mode
 - `npm install`
 - `npm start`

## Build for production
 - `npm install`
 - `npm run build`

## Architecture
 - Layered architecture
 - Reusable components
 - Simple and clean codebase
 - Code is designed by DRY principle

## Project structure (src directory)
 - action/ - Redux actions
 - component/ - Simple components
 - container/ - Smart (Redux) components that can contains simple visual logic
 - helper/ - Utils & Helpers
 - page/ - Stateless components only to group smart components by pages
 - reducer/ - Redux reducers
 - service/ - Business logic layer

## Technologies
 - [React](https://reactjs.org/) - library used as presentation layer
 - [Redux](https://redux.js.org/) - state manager used as model layer
 - [Semantic UI React](https://react.semantic-ui.com/introduction) - Minimalist react components
 - [Axios](https://github.com/axios/axios) - XHR adapter
 - [Webpack](https://webpack.js.org/) - as build tool