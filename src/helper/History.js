import createBrowserHistory from 'history/createBrowserHistory';

class History {
    
    api = createBrowserHistory({
        basename: '',
        forceRefresh: false,
        keyLength: 6
    });

    forward(path) {
        this.api.push(path);
    }
    
    back() {
        this.api.goBack();
    }
}

var instance = null;
History.getInstance = function() {
    if (instance === null) {
        instance = new History();
    }
    return instance;
}

export default History;
