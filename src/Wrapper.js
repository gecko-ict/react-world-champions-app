import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import History from './helper/History';
import IndexPage from './page/IndexPage';
import YearStatsPage from './page/YearStatsPage';
import NotFoundPage from './page/NotFoundPage';
import configureReduxStore from './configureReduxStore';
import { Provider } from 'react-redux';

export default class Wrapper extends Component {

    history = History.getInstance();

    render() {
        const reduxStore = configureReduxStore();
        return (
            <Provider store={reduxStore}>
                <Router history={this.history.api}>
                    <Switch>
                        <Route exact path="/" component={IndexPage} />
                        <Route exact path="/year/:year" component={YearStatsPage} />
                        <Route exact component={NotFoundPage} />
                    </Switch>
                </Router>
            </Provider>
        );
    }
};
