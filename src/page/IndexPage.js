import React from 'react';
import { Container, Header, Icon } from 'semantic-ui-react';
import HeaderContainer from '../container/HeaderContainer';

export default () => {
    return (
        <Container>
            <HeaderContainer />
            <Header textAlign='center' as='h2' icon>
                <Icon name='arrow up' />
                <Header.Subheader>Select year and see results.</Header.Subheader>
            </Header>
        </Container>
    );
};