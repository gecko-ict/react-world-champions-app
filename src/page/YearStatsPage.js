import React from 'react';
import { Container } from 'semantic-ui-react';
import HeaderContainer from '../container/HeaderContainer';
import ErgastByYearContainer from '../container/ErgastByYearContainer';

export default ({ match }) => {
    return (
        <Container>
            <HeaderContainer />
            <ErgastByYearContainer defaultYear={match.params.year} />
        </Container>
    );
};