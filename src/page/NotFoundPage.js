import React from 'react';
import { Container } from 'semantic-ui-react';
import HeaderContainer from '../container/HeaderContainer';
import ErrorContainer from '../container/ErrorContainer';

export default () => {
    return (
        <Container>
            <HeaderContainer />
            <ErrorContainer />
        </Container>
    );
};