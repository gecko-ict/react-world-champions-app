import React from 'react';
import ReactDOM from 'react-dom';
import Wrapper from './Wrapper';
import 'semantic-ui-css/semantic.min.css';
import 'es5-shim';
import 'es5-shim/es5-sham';
import 'console-polyfill';

ReactDOM.render(<Wrapper />, document.getElementById('root'));
