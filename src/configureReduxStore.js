import { createStore, applyMiddleware, combineReducers } from 'redux';
import rootReducer from './reducer/rootReducer';
import thunk from 'redux-thunk';

export default function configureStore() {
    return createStore(
        combineReducers({
            root: rootReducer
        }),
        applyMiddleware(thunk)
    );
}
