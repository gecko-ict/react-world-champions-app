import reduxInitialState from '../reduxInitialState';
import { YEAR_CHANGED, ERGAST_DATA_FETCHED, ERGAST_DATA_FETCHING_FAILED } from '../reduxActionTypes';

export default (state, { type, payload }) => {
    state = { ...state, ...reduxInitialState };
    if (type === YEAR_CHANGED) {
        state.year = payload;
    }

    if (type === ERGAST_DATA_FETCHED) {
        state.year = payload.year;
        state.ergast = payload.data;
    }

    if (type === ERGAST_DATA_FETCHING_FAILED) {
        state.year = payload.year;
        state.error = payload.error;
    }

    return state;
}