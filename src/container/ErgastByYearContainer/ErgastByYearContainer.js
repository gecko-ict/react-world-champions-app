import React, { Component } from 'react';
import { Grid, Header } from 'semantic-ui-react';
import ErgastTableComponent from '../../component/ErgastTableComponent';

export default class ErgastByYearContainer extends Component {
    
    componentWillMount() {
        if (this.props.year === null) {
            this.props.changeYear(this.props.defaultYear);
        }
    }
    
    render() {
        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column>
                        <Header as='h2' icon='winner' content={'Season winner: ' + (this.props.ergastData.winner || 'No Data')} />
                        <ErgastTableComponent list={this.props.ergastData.list} winner={this.props.ergastData.winner} />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
};