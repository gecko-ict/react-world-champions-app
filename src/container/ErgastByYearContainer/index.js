import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ErgastByYearContainer from './ErgastByYearContainer';
import changeYearAction from '../../action/changeYearAction';

let mapState = (state) => {
    return {
        ergastData: state.root.ergast,
        year: state.root.year
    };
}

let mapActions = (dispatch) => {
    return {
        changeYear: bindActionCreators(changeYearAction, dispatch)
    };
}

export default connect(mapState, mapActions)(ErgastByYearContainer);