import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import HeaderContainer from './HeaderContainer';
import changeYearAction from '../../action/changeYearAction';

let mapState = (state) => {
    return {
        year: state.root.year
    };
}

let mapActions = (dispatch) => {
    return {
        changeYear: bindActionCreators(changeYearAction, dispatch)
    };
}

export default connect(mapState, mapActions)(HeaderContainer);