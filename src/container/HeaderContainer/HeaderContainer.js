import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import NavigationComponent from '../../component/NavigationComponent';
import BrandComponent from '../../component/BrandComponent';

export default class HeaderContainer extends Component {

    onYearChange = (year) => {
        this.props.changeYear(year);
    };

    render() {
        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column>
                        <BrandComponent />
                        <NavigationComponent active={this.props.year} onChange={this.onYearChange} />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }

}