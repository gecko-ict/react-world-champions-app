import React, { Component } from 'react';
import { Header, Icon } from 'semantic-ui-react';

export default class PageNotFoundContainer extends Component {
    render() {
        return (
            <Header as='h2' icon textAlign='center'>
                <Icon name='window close outline' color='red' />
                Oops!
                <Header.Subheader>
                    {this.props.error !== null ? this.props.error : "We can't seem to find the page you're looking for."}
                </Header.Subheader>
            </Header>

        );
    }
};