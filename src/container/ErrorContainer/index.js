import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ErrorContainer from './ErrorContainer';
import changeYearAction from '../../action/changeYearAction';

let mapState = (state) => {
    return {
        year: state.root.year,
        error: state.root.error
    };
}

let mapActions = (dispatch) => {
    return {
        changeYear: bindActionCreators(changeYearAction, dispatch)
    };
}

export default connect(mapState, mapActions)(ErrorContainer);