import { YEAR_CHANGED, ERGAST_DATA_FETCHED, ERGAST_DATA_FETCHING_FAILED } from '../reduxActionTypes';
import History from '../helper/History';
import ErgastDataService from '../service/ErgastDataService';

export default (year) => {

    let history = History.getInstance();
    let ergastDataService = new ErgastDataService();

    return dispatch => {
        history.forward('/year/' + year);
        dispatch({type: YEAR_CHANGED, payload: year});
        ergastDataService.getData(year).then((data) => {
            dispatch({type: ERGAST_DATA_FETCHED, payload: { data, year }});
        }).catch((e) => {
            history.forward('/error');
            dispatch({type: ERGAST_DATA_FETCHING_FAILED, payload: { year: year , error: e.message }});
        });
    };
}