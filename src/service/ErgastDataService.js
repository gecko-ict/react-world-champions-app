import axios from 'axios';

export default class ErgastDataService {

    getData(year) {
        let data = sessionStorage.getItem('ergast-data-' + year);
        if (data === null) {
            return axios.get(`http://ergast.com/api/f1/${year}/results.json?limit=500&offset=0`).then((data) => {
                let filteredData = this.dataFilter(data);    
                sessionStorage.setItem('ergast-data-' + year, JSON.stringify(filteredData));
                return filteredData; 
            });
        } else {
            return new Promise((resolve) => {
                resolve(JSON.parse(data));
            });
        }
    }

    dataFilter(results) {
        let list = []
            let data = results.data.MRData.RaceTable.Races
            let seasonWinningMap = {}
            for (let i = 0; i < data.length; i++) {
                let raceWinningMap = {};
                let race = {
                    raceName: data[i].raceName,
                    winner: null
                };
                for (let j = 0; j < data[i].Results.length; j++) {
                    let name = data[i].Results[j].Driver.givenName + ' ' + data[i].Results[j].Driver.familyName;
                    if (typeof seasonWinningMap[name] === 'undefined') {
                        seasonWinningMap[name] = parseInt(data[i].Results[j].points, 10)
                    } else {
                        seasonWinningMap[name] += parseInt(data[i].Results[j].points, 10)
                    }
                    if (typeof raceWinningMap[name] === 'undefined') {
                        raceWinningMap[name] = parseInt(data[i].Results[j].points, 10)
                    } else {
                        raceWinningMap[name] += parseInt(data[i].Results[j].points, 10)
                    }
                }
                let raceTop = 0
                let raceWinner = null
                for (var prop in raceWinningMap) {
                    if (raceWinningMap[prop] > raceTop) {
                        raceTop = raceWinningMap[prop]
                        raceWinner = prop
                    }
                }
                race.winner = raceWinner;
                list.push(race)
            }
            let top = 0
            let winner = null
            for (let prop in seasonWinningMap) {
                if (seasonWinningMap[prop] > top) {
                    top = seasonWinningMap[prop]
                    winner = prop
                }
            }

            return { list, winner };
    }

}