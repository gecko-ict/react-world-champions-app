import React from 'react';
import { Menu } from 'semantic-ui-react';

export default ({ active = null, onChange }) => { 
    let menuItemList = [];
    let menuItemClick = (e, { name }) => {
        onChange(name);
    };
    for (let i = 2005; i <= 2015; i++) {
        menuItemList.push(
            <Menu.Item key={i} name={i + ''} active={active === i.toString()} onClick={menuItemClick} />
        );
    }
    return (<Menu pointing>{menuItemList}</Menu>);
}