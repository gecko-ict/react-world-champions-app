import React from 'react';
import { Header, Icon } from 'semantic-ui-react';
import './brandComponent.css';

export default () => { 
    return (
        <Header className="brand-component" as='h2' icon textAlign='center'>
            <Icon name='road' circular />
            <Header.Content>F1 World Championship App</Header.Content>
        </Header>
    );
}