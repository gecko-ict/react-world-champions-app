import React from 'react';
import { Table } from 'semantic-ui-react';
import './ergastTableComponent.css';

export default ({ list = [], winner }) => {

    return (
        <Table celled className="ergast-table-component">
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Winner</Table.HeaderCell>
                    <Table.HeaderCell>Race Name</Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
                {list.map((item) => {
                    return (
                        <Table.Row key={item.raceName} className={item.winner === winner ? 'winner': ''}>
                        <Table.Cell>{item.winner}</Table.Cell>
                            <Table.Cell>{item.raceName}</Table.Cell>
                        </Table.Row>
                    )
                })}
            </Table.Body>
        </Table>
    );
}